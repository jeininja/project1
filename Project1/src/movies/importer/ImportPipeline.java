package movies.importer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This class contains the main method with a filled Processor[]
 * @author Mikael Baril
 * @author Jei wen Wu
 */
public class ImportPipeline {
	
	public static void main (String[] args) throws IOException {
		
		Processor[] processor = new Processor[5];
		
		String source = "C:\\Users\\jei_w\\Documents\\Pipeline\\KaggleInput";
		String destination = "C:\\Users\\jei_w\\Documents\\Pipeline\\Importer";
		
		KaggleImporter kagImp = new KaggleImporter(source, destination);
		processor[0] = kagImp;
		
		String source2 = "C:\\Users\\jei_w\\Documents\\Pipeline\\ImdbInput";
		String destination2 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Importer";
		
		ImdbImporter imdbImp = new ImdbImporter(source2,destination2);
		processor[1] = imdbImp;
		
		String source3 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Importer";
		String destination3 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Normalizer";
		
		Normalizer normalizer = new Normalizer(source3, destination3);
		processor[2] = normalizer;
		
		String source4 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Normalizer";
		String destination4 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Validator";
		
		Validator validator = new Validator(source4,destination4);
		processor[3] = validator;
		
		String source5 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Validator";
		String destination5 = "C:\\Users\\jei_w\\Documents\\Pipeline\\Deduper";
		
		Deduper deduper = new Deduper(source5, destination5);
		processor[4] = deduper;
		
		processAll(processor);
		
	}
	
	//Runs the processors inside the Processor[] 1 by 1
	public static void processAll(Processor[] processorInput) throws IOException {
		
		for(Processor p : processorInput) {
			p.execute();
		}
		
	}
	
}
