package movies.importer;

import java.util.ArrayList;
	/**
	 * This class extends the Processor class and overrides the process method to do something more specific
	 * @author Mikael Baril
	 * @author Jei wen Wu
	 */
public class Validator extends Processor{
	private final int yearColumn = 0;
	private final int durationColumn = 1;
	private final int nameColumn = 3;
	
	//This constructor is inherited from the Processor class
	public Validator(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	//This is a overloaded constructor that only takes 2 inputs with boolean srcContainsHeader set to false
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * Override of the method process so it does a specific version of process
	 * Should be used after executing Normalizer object only 
	 * Validates the Movie objects by removing movies with either an empty year, empty runtime or empty name
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> output = new ArrayList<String>();
		
		for (String i : input) {
			String[] splits= i.split("\\t",-1); 

            if(splits[yearColumn]!= null && splits[yearColumn].length()!=0 && splits[durationColumn]!= null && splits[durationColumn].length()!=0 && splits[nameColumn]!= null && splits[nameColumn].length()!=0) {
            	output.add(i);
            	try {
            		Integer.parseInt(splits[yearColumn]);
            		Integer.parseInt(splits[durationColumn]);
                }
                catch(NumberFormatException e){
                	System.out.println("A NumberFormatException has been caught");
                }
            }
		}
		return output;
	}
}
