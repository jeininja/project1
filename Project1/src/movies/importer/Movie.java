package movies.importer;

/**
 * This class create a Movie object with methods that can be used on Movie objects
 * @author Jei Wen Wu
 */
public class Movie {
	private String releaseYear;
	private String name;
	private String runTime;
	private String source;

	//Getter methods for the Movie object fields
	public String getReleaseYear() {
		return this.releaseYear;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getRunTime() {
		return this.runTime;
	}
	
	public String getSource() {
		return this.source;
	}
	
	//Overriding the toString() method so that it is more specific to Movie objects by printing Movie related fields separated by tabs
	@Override
	public String toString() {
		String result = getReleaseYear()+'	'+getRunTime()+'	'+getSource()+'	'+getName();
		return result;
	}
	
	//Movie constructor to create a Movie, takes as input 4 Strings representing the releaseYear, runtime, source and name
	public Movie(String releaseYear, String runTime, String source, String name) {
		
		this.releaseYear = releaseYear;
		this.name = name;
		this.runTime = runTime;
		this.source = source;
		
	}
	
	/**
	 * Overriding the equals() method from the Object class to a more specific one for the Movie objects
	 * Movies are considered to be equal if they have the exact same title and release year and can differ in runtime by 5 minutes
	 */
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Movie)) {
			return false;
		}
		else if(this.getReleaseYear().equals(((Movie)o).getReleaseYear()) && this.getName().equals(((Movie)o).getName()) && ((Movie)o).getRunTime().length()!=0 && this.getRunTime().length()!=0) {
			
			//Parsing the runTimes into int so that it is possible to perform math operations with them
			int thisRunTime = Integer.parseInt(this.getRunTime());
			int objRunTime = Integer.parseInt(((Movie)o).getRunTime());
			
			int runTimeDiff = thisRunTime - objRunTime;
			
			//If the runTimes are different by <=5, the movies are considered equal. Else they are not equal
			if(Math.abs(runTimeDiff) <= 5) 
				return true;
			else {
				return false;
			}
			}
		else {
			return false;
		}
		}
}
