package movies.importer;

import java.util.*;

	/**
	 * This class extends the Processor class and overrides the process method to do something more specific
	 * @author Jei wen Wu
	 */
public class KaggleImporter extends Processor {
	
	private final int year = 20;
	private final int runtime = 13;
	private final int title = 15;
	private final String source = "kaggle";
	
	//This constructor is inherited from the Processor class
	public KaggleImporter(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	//This is a overloaded constructor that only takes 2 inputs with boolean srcContainsHeader set to true
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	/**
	 * Override of the method process so it does a specific version of process
	 * Takes specific informations about the input file and creates Movie objects with them
	 * Uses the Movie.toString() method to change it to a String and adds it to the ArrayList<String>
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList<String> output = new ArrayList<String>();
		
		for(String s : input) {
			
			Movie m = new Movie(this.getYear(s), this.getRunTime(s), source, this.getTitle(s));
			String movieInfo = m.toString();
			
			output.add(movieInfo);
			
		}
		
		return output;
	}
	
	//Get methods to retrieve a specific element in a given String
	public String getYear(String s) {
		
		String [] split = s.split("\\t");
		
		return split[year];
	}
	
	//Get the runtime if it is available, else return an empty string
	public String getRunTime(String s) {
		
		 String [] split = s.split("\\t");
		 
		 String empty = "";
		 String runTimeChecker = "Runtime Not Available";
		 
		 if(split[runtime].equals(runTimeChecker)){
			 return empty;
		 }
		 
		 return split[runtime];
	}
	
	public String getTitle(String s) {
		
		 String [] split = s.split("\\t");
		 
		 return split[title];
	}
}
