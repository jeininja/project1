package movies.importer;

import java.util.ArrayList;

	/**
	 * This class extends the Processor class and overrides the process method to do something more specific
	 * @author Jei wen Wu
	 */
public class Normalizer extends Processor{
	
	private final int year = 0;
	private final int source = 2;
	private final int title = 3;
	
	//This constructor is inherited from the Processor class
	public Normalizer(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	//This is a overloaded constructor that only takes 2 inputs with boolean srcContainsHeader set to false
	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * Override of the method process so it does a specific version of process
	 * Should be used after executing KaggleImporter or ImdbImporter object only 
	 * Normalizes the Movie objects by converting the titles to lower case letters and only keeping the minutes from the Runtime 
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> output = new ArrayList<String>();
		
		for (String s : input) {
			
			String[] split = s.split("\\t");
			
			String lowerCase = this.getTitle(s).toLowerCase();
			
			String[] runTimeSplit = split[1].split(" ");
			String numberOnly = runTimeSplit[0];
			
			Movie m = new Movie(this.getYear(s),numberOnly, this.getSource(s), lowerCase);
			String normalized = m.toString();
			
			output.add(normalized);
			
		}
		
		return output;
	}
	
	//Get methods to retrieve a specific element in a given String
	public String getYear(String s) {
		
		String [] split = s.split("\\t");
		
		return split[year];
	}
	
	public String getSource(String s) {
		
		String [] split = s.split("\\t");
		
		return split[source];
	}
	
	public String getTitle(String s) {
		
		String [] split = s.split("\\t");
		
		return split[title];
	}

}
