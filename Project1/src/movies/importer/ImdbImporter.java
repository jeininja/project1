
package movies.importer;
import java.util.*;

	//@Author Mikael Baril
public class ImdbImporter extends Processor{
	/*private final int yearColumn = 3;
	private final int durationColumn = 6;
	private final int nameColumn = 1;*/
	//Making the super Constructor from Processor code
	public ImdbImporter(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	//Hardcoding true
	public ImdbImporter(String sourceDir, String outputDir) {
	
		super(sourceDir, outputDir, true);
	}
	/*Making array list to store the values from text file and output it
	in the correct format*/
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> output = new ArrayList<String>();
		
		for(String i : input ) {
			
			String [] splits = i.split("\\t");
			/*String title = splits[nameColumn];
			String year = splits[yearColumn];
			String duration = splits[durationColumn];*/
			Movie format = new Movie (splits[3],splits[6],"imdb",splits[1]);
			
			String imdbInfo = format.toString();
			
			output.add(imdbInfo);
		}
	return output;
	}
	
}
