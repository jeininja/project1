package movies.importer;

import java.util.ArrayList;
import java.util.Objects;

/**
 * This class removes duplicate Movies based on their name, release year and runtime
 * @author Mikael Baril
 * @author Jei wen Wu
 */
public class Deduper extends Processor{
	
	private final int year = 0;
	private final int runtime = 1;
	private final int source = 2;
	private final int title = 3;

	public Deduper(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * Override of the method process so it does a specific version of process
	 * Creates a new ArrayList<Movie> to be able to use the contains() with override equals()
	 * Converts the ArrayList<Movie> back to a ArrayList<String> in order to return it
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<Movie> output = new ArrayList<Movie>();
		
		for(String s : input) {
			
			String[] split = s.split("\\t");
			Movie m = new Movie(split[year],split[runtime],split[source],split[title]);
			
			boolean dup = output.contains(m);
			
			if(dup == false) {
				output.add(m);
			}
			else {
				
				int dupLine = 0;
				
				for(int j = 0; j<output.size();j++) {
					if(output.get(j).equals(m)) {
						dupLine=j;
					}
				}
				
				Movie duplicatedMovie = output.get(dupLine);;
				
				String sourceHolder = "";
				
				if(split[source].equals(duplicatedMovie.getSource())) {	
					sourceHolder = split[source];
				}
				else {
					sourceHolder = "kaggle;imdb";
				}
				
				Movie merged = new Movie(split[year], split[runtime], sourceHolder, split[title]);
				
				output.set(dupLine, merged);
				
			}
		}
		
		ArrayList<String> stringArrayList = new ArrayList<>(output.size());
		
		for (Movie movieObj : output) {
		    stringArrayList.add(movieObj.toString());
		}
		
		return stringArrayList;
		
	}
}

