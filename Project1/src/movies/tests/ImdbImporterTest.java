package movies.tests;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

import movies.importer.ImdbImporter;


class ImdbImporterTest {

	/*void test() throws IOException{
		
		ArrayList<String> fileI = new ArrayList<String>();
		fileI.add("imdb_title_id\ttitle\toriginal_title\tyear\tdate_published\tgenre\tduration\tcountry\tlanguage\tdirector\twriter\tproduction_company\tactors\tdescription\tavg_vote\tvotes\tbudget\tusa_gross_income\tworlwide_gross_income\tmetascore\treviews_from_users\treviews_from_critics");
		fileI.add("tt0000009\tMiss Jerry\tMiss Jerry\t1894\t1894-10-09\tRomance\t45\tUSA\tNone\tAlexander Black\tAlexander Black\tAlexander Black Photoplays\t\"Blanche Bayliss, William Courtenay, Chauncey Depew\"\tThe adventures of a female reporter in the 1890s.\t5.9\t154\t1\t2");
		fileI.add("tt0000574\tThe Story of the Kelly Gang\tThe Story of the Kelly Gang\t1906\t12/26/1906\t\"Biography, Crime, Drama\"\t70\tAustralia\tNone\tCharles Tait\tCharles Tait\tJ. and N. Tait\t\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"\tTrue story of notorious Australian outlaw Ned Kelly (1855-80).\t6.1\t589\t\"$2,250 \"\t7\t7");
		
		ArrayList<String> expectedI = new ArrayList<String>();
		expectedI.add("title\tyear\tsource\tduration");
		expectedI.add("Miss Jerry\t1894\tImdb\t45");
		expectedI.add("The Story of the Kelly Gang\t1906\tImdb\t70");
		
		
		String source = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Project1";
		String destination = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Project1\\MovieOutput";
				
		ImdbImporter importer = new ImdbImporter(source,destination);
		importer.process(fileI);
		assertEquals(expectedI,importer.process(fileI));
			
			}*/
	@Test
	void yearTest() throws IOException{
		String testY = "tt0000009\tMiss Jerry\tMiss Jerry\t1894\t1894-10-09\tRomance\t45\tUSA\tNone\tAlexander Black\tAlexander Black\tAlexander Black Photoplays\t\"Blanche Bayliss, William Courtenay, Chauncey Depew\"\tThe adventures of a female reporter in the 1890s.\t5.9\t154\t1\t2";
		String [] splits = testY.split("\\t");
		
		String year = splits[3];
		String expectedY="1894";
		assertEquals(expectedY,year);
		
		
	}
	@Test
	void titleTest() throws IOException{
		String testT = "tt0000009\tMiss Jerry\tMiss Jerry\t1894\t1894-10-09\tRomance\t45\tUSA\tNone\tAlexander Black\tAlexander Black\tAlexander Black Photoplays\t\"Blanche Bayliss, William Courtenay, Chauncey Depew\"\tThe adventures of a female reporter in the 1890s.\t5.9\t154\t1\t2";
		String [] splits = testT.split("\\t");
		
		String title = splits[1];
		String expectedT="Miss Jerry";
		assertEquals(expectedT,title);
		
		
	}
	@Test
	void runtimeTest() throws IOException{
		String testR = "tt0000009\tMiss Jerry\tMiss Jerry\t1894\t1894-10-09\tRomance\t45\tUSA\tNone\tAlexander Black\tAlexander Black\tAlexander Black Photoplays\t\"Blanche Bayliss, William Courtenay, Chauncey Depew\"\tThe adventures of a female reporter in the 1890s.\t5.9\t154\t1\t2";
		String [] splits = testR.split("\\t");
		
		String runtime = splits[6];
		String expectedR="45";
		assertEquals(expectedR,runtime);
		
		
	}
		
	}


