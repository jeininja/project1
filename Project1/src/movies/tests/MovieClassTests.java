//This is a JUnit test class and should test all the methods from the Movie class

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

/**
 * Test class used to test the methods and conditions inside the Movie class
 * @author Jei Wen Wu
 */
class MovieClassTests {

	//JUnit test method to test the getter methods from the Movie class
	@Test
	void testGets() {
		Movie movie = new Movie("2010", "162minutes", "RottenTomatoes","Inception");
		assertEquals("2010", movie.getReleaseYear());
		assertEquals("162minutes", movie.getRunTime());
		assertEquals("RottenTomatoes", movie.getSource());
		assertEquals("Inception", movie.getName());
	}
	
	//JUnit test method to test the toString() method from the Movie class
	@Test
	void testToString() {
		Movie movie = new Movie("2010", "162minutes", "RottenTomatoes", "Inception");
		String expected = "2010	162minutes	RottenTomatoes	Inception";
		
		assertEquals(expected, movie.toString());
	}
	
	//This test if equals() returns true if everything is the exact same
	@Test 
	void TestEquals(){
		Movie movie1 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		
		assertEquals(true, movie1.equals(movie2));
	}
	
	//This test if equals() returns true if everything is the same besides the runtime by under 5 minutes
	@Test 
	void TestEqualsRunTime1(){
		Movie movie1 = new Movie("2010", "160", "Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "163", "Rotten Tomatoes","Inception");
		
		assertEquals(true, movie1.equals(movie2));
	}
	
	//This test if equals() returns true if everything is the same besides the runtime by 5 minutes
	@Test 
	void TestEqualsRunTime2(){
		Movie movie1 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "167", "Rotten Tomatoes","Inception");
		
		assertEquals(true, movie1.equals(movie2));
	}
	
	//This test if equals() returns false if everything is the same but the runtime is different by over 5 minutes
	@Test 
	void TestEqualsRunTime3(){
		Movie movie1 = new Movie("2010", "160", "Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "167", "Rotten Tomatoes","Inception");
		
		assertEquals(false, movie1.equals(movie2));
	}
	
	//This test if equals() returns true if everything is the same besides the source
	@Test 
	void TestEqualsSource(){
		Movie movie1 = new Movie("2010", "162", "Not Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		
		assertEquals(true, movie1.equals(movie2));
	}
	
	//This test if equals() returns false if everything is the same but besides the title
	@Test 
	void TestEqualsTitle(){
		Movie movie1 = new Movie("2010", "162", "Rotten Tomatoes","NotInception");
		Movie movie2 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		
		assertEquals(false, movie1.equals(movie2));
	}

	//This test if equals() returns false if everything is the same besides the year
	@Test 
	void TestEqualsYear(){
		Movie movie1 = new Movie("1999", "162", "Rotten Tomatoes","Inception");
		Movie movie2 = new Movie("2010", "162", "Rotten Tomatoes","Inception");
		
		assertEquals(false, movie1.equals(movie2));
	}
	
}
