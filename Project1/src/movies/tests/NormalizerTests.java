package movies.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;
import movies.importer.Normalizer;

/**
 * Test class used to test the methods inside the Normalizer class
 * @author Jei wen Wu
 */
class NormalizerTests {

	//JUnit test method used to test the processor method from Normalizer class
	@Test
	void testProcessor() throws IOException {
		
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> expected = new ArrayList<String>();
		
		input.add("2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor");
		input.add("2016\t111 minutes\tkaggle\tThe Masked Saint");
		input.add("1996\t80 minutes\tkaggle\tSpy Hard");

		
		expected.add("2008\t112\tkaggle\tthe mummy: tomb of the dragon emperor");
		expected.add("2016\t111\tkaggle\tthe masked saint");
		expected.add("1996\t80\tkaggle\tspy hard");
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Normalized";

		Normalizer normalizer = new Normalizer(source, output);
		
		assertEquals(expected, normalizer.process(input));
	}
	
	//This method tests if it only keeps the numbers without the minutes correctly
	@Test
	void testRemovedMinutes(){
		
		String testString = "2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor";
		String[] split = testString.split("\\t");
		
		String[] runTimeSplit = split[1].split(" ");
		String minutes = runTimeSplit[0];
		
		String expected = "112";
		
		assertEquals(expected, minutes);
		
	}
	
	//This method tests if it changes the title to lower case letters correctly
	@Test
	void testLowerCase(){
	
		String testString = "2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor";
		String[]split = testString.split("\\t");
		
		String lowerCase = split[3].toLowerCase();
		
		String expected = "the mummy: tomb of the dragon emperor";
		
		assertEquals(expected, lowerCase);
		
	}
	
	//This method tests if it can retrieve the year correctly
	@Test
	void testGetYear(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor";
		String expected = "2008";
		
		Normalizer normalizer = new Normalizer(source, output);
		
		assertEquals(expected, normalizer.getYear(testString));
		
	}
	
	//This method tests if it can retrieve the source correctly 
	@Test
	void testGetSource(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor";
		String expected = "kaggle";
		
		Normalizer normalizer = new Normalizer(source, output);
		
		assertEquals(expected, normalizer.getSource(testString));
		
	}
	
	//This method tests if it can retrieve the title correctly 
	@Test
	void testGetTitle(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor";
		String expected = "The Mummy: Tomb of the Dragon Emperor";
		
		Normalizer normalizer = new Normalizer(source, output);
		
		assertEquals(expected, normalizer.getTitle(testString));
		
	}
	
}

