package movies.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;

/**
 * Test class used to test various things about the KaggleImporter class
 * @author Jei wen Wu
 */
class KaggleImporterTests {

	
	//JUnit test method used to test the processor method from KaggleImporter class (tests cases grouped into 1 big test)
	@Test
	void testProcessor() throws IOException {
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> expected = new ArrayList<String>();
		
		
		//input.add("Cast 1	Cast 2	Cast 3	Cast 4	Cast 5	Cast 6	Description	Director 1	Director 2	Director 3	Genre	Rating	Release Date	Runtime	Studio	Title	Writer 1	Writer 2	Writer 3	Writer 4	Year");
		input.add("Brendan Fraser\tJohn Hannah\tMaria Bello\tMichelle Yeoh\tJet Li\tRussell Wong\t\"A very long description\"\tRob Cohen\tSimon Duggan\tDirector Not Available\tAction\tPG-13\t7/24/2008\t112 minutes\tUniversal Pictures\tThe Mummy: Tomb of the Dragon Emperor\tAlfred Gough\tMiles Millar\tWriter Not Available\tWriter Not Available\t2008");
		input.add("Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\t\"A medium size description\"\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13 \t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff	Writer Not Available\tWriter Not Available\t2016");
		input.add("Leslie Nielsen\tNicollette Sheridan\tAndy Griffith\tMarcia Gay Harden\tJohn Ales\tBarry Bostwick\t\"Description\"\tRick Friedberg\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t5/24/1996\t80 minutes\tHollywood Pictures\tSpy Hard\tRick Friedberg\tDick Chudnow\tJason Friedberg\tAaron Seltzer\t1996");	
		
		expected.add("2008\t112 minutes\tkaggle\tThe Mummy: Tomb of the Dragon Emperor");
		expected.add("2016\t111 minutes\tkaggle\tThe Masked Saint");
		expected.add("1996\t80 minutes\tkaggle\tSpy Hard");
		
		KaggleImporter kagImp = new KaggleImporter(source, output);
		
		assertEquals(expected, kagImp.process(input));
	}
	
	//This method tests if it can retrieve the year correctly
	@Test
	void testGetYear(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "C1\tC2\tC3\tC4\tC5\tC6\t\"A very long description\"\tD1\tD2\tD3\tGenre\tRating\tReleaseDate\t112 minutes\tStudio\tInception\tW1\tW2\tW3\tW4\t2000";
		String expected = "2000";

		KaggleImporter KI = new KaggleImporter(source, output);
		
		assertEquals(expected, KI.getYear(testString));
		
	}
	
	//This method tests if it can retrieve the runtime with the minutes correctly
	@Test
	void testRunTime(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "C1\tC2\tC3\tC4\tC5\tC6\t\"A very long description\"\tD1\tD2\tD3\tGenre\tRating\tReleaseDate\t112 minutes\tStudio\tInception\tW1\tW2\tW3\tW4\t2000";
		String expected = "112 minutes";
		
		KaggleImporter KI = new KaggleImporter(source, output);
		
		assertEquals(expected, KI.getRunTime(testString));
		
	}
	
	//This method tests if it can retrieve the title correctly 
	@Test
	void testGetTitle(){
		
		String source = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Input" ;
		String output = "C:\\Users\\jei_w\\Documents\\Programming III\\Filter\\Output";
		String testString = "C1\tC2\tC3\tC4\tC5\tC6\t\"A very long description\"\tD1\tD2\tD3\tGenre\tRating\tReleaseDate\t112 minutes\tStudio\tInception\tW1\tW2\tW3\tW4\t2000";
		String expected = "Inception";
		
		KaggleImporter KI = new KaggleImporter(source, output);
		
		assertEquals(expected, KI.getTitle(testString));
		
	}

}
